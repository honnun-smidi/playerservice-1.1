drop table playerfantasypoints;
create table playerfantasypoints
(
  playerId int NOT NULL,
  fantasyPoints float NOT NULL
)