select p.playerid, p.firstname, p.lastname, p.height,
  p.weight, p.birthdate, p.countryid, p.teamid,
  c.countryid, c.name AS countryname,
  c.abbreviation AS countryabbreviation,
  po.positionid, po.name AS positionname,
  po.abbreviation AS positionabbreviation
from players p
  join playerpositions pp on p.playerid = pp.playerid
  join positions po on pp.positionid = po.positionid
  join countries c on p.countryid = c.countryid
where pp.positionid = 2;