package is.rufan.player.process;

import is.rufan.player.domain.PlayerFantasyPoints;
import is.rufan.player.service.PlayerService;
import is.rufan.player.service.PlayerServiceException;
import is.ruframework.process.RuAbstractProcess;
import is.ruframework.reader.RuReadHandler;
import is.ruframework.reader.RuReader;
import is.ruframework.reader.RuReaderException;
import is.ruframework.reader.RuReaderFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSource;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.logging.Logger;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class PlayerFantasyPointsImportProcess extends RuAbstractProcess implements RuReadHandler {
    private PlayerService playerService;
    MessageSource msg;
    Logger logger = Logger.getLogger(this.getClass().getName());

    @Override
    public void beforeProcess() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:playerapp.xml");
        playerService = (PlayerService) ctx.getBean("playerService");
        msg = (MessageSource) ctx.getBean("messageSource");
        logger.info("processbefore: " + getProcessContext().getProcessName());
    }

    @Override
    public void startProcess() {
        RuReaderFactory factory = new RuReaderFactory("playerfantasypointsprocess.xml");
        RuReader reader = factory.getReader("playerFantasyPointsReader");
        logger.info("processstart");

        reader.setReadHandler(this);
        try {
            reader.read();
        } catch (RuReaderException rrex) {
            String errorMsg = "Unable to read specified file";
            logger.severe(errorMsg);
        }
    }

    @Override
    public void afterProcess() {
        logger.info("afterprocess");
    }

    public void read(int count, Object object) {
        PlayerFantasyPoints playerPoints = (PlayerFantasyPoints) object;
        try {
            playerService.addPlayerFantasyPoints(playerPoints);
        } catch (PlayerServiceException psex) {
            logger.warning("Player fantasy points for player id " + playerPoints.getPlayerId() + " not added " + psex.getMessage());
        }
    }
}
