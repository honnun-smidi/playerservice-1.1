package is.rufan.player.process;

import is.rufan.player.domain.PlayerFantasyPoints;
import is.ruframework.reader.RuAbstractReader;
import is.ruframework.reader.RuJsonUtil;
import is.ruframework.reader.RuReaderException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class PlayerFantasyPointsReader extends RuAbstractReader {
    public Object parse(String content) throws RuReaderException {
        // Root object
        JSONArray jPointsArray = (JSONArray) JSONValue.parse(content);

        List<PlayerFantasyPoints> playerPoints = new ArrayList<PlayerFantasyPoints>();
        PlayerFantasyPoints playerFantasyPoints;
        for (int i = 0; i < jPointsArray.size(); i++) {
            playerFantasyPoints = new PlayerFantasyPoints();
            JSONObject jPlayerPoints = (JSONObject) jPointsArray.get(i);
            playerFantasyPoints.setPlayerId(RuJsonUtil.getInt(jPlayerPoints, "PlayerId"));
            playerFantasyPoints.setFantasyPoints(((Number) jPlayerPoints.get("FantasyPoints")).floatValue());

            playerPoints.add(playerFantasyPoints);
            readHandler.read(i, playerFantasyPoints);
        }

        return playerPoints;
    }
}
