package is.rufan.player.test;

import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.player.service.PlayerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.List;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class TestPlayerPositions {
    public static void main(String[] args) {
        new TestPlayerPositions();
    }

    public TestPlayerPositions() {
        ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:playerapp.xml");
        PlayerService playerService = (PlayerService) ctx.getBean("playerService");

        List<PlayerWithTeamName> players = playerService.getPlayersByPosition(2);

        System.out.println("Players");
        for (Player p : players) {
            System.out.println(p);
        }
    }

}
