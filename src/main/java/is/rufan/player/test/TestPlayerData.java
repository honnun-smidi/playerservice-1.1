package is.rufan.player.test;

import is.rufan.player.domain.Country;
import is.rufan.player.domain.Player;
import is.rufan.player.service.PlayerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import java.util.Date;

public class TestPlayerData
{
  public TestPlayerData()
  {

    ApplicationContext applicationContext = new FileSystemXmlApplicationContext("classpath:playerapp.xml");
    PlayerService playerService = (PlayerService)applicationContext.getBean("playerService");

    System.out.println(playerService.getPlayer(345196));
    System.out.println(playerService.getPlayerFantasyPoints(345196).getFantasyPoints());
  }

  public static void main(String[] args)
  {
    new TestPlayerData();
  }
}
