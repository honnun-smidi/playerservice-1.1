package is.rufan.player.domain;


import java.util.Date;
import java.util.List;

/**
 * Created by Snaebjorn on 10/26/2015.
 */
public class PlayerWithTeamName extends Player {
    private String teamName;

    public PlayerWithTeamName() {
    }

    public PlayerWithTeamName(int playerId, String firstName, String lastName, int height, int weight, Date birthDate, Country nationality, int teamId, Position position, String teamName) {
        super(playerId, firstName, lastName, height, weight, birthDate, nationality, teamId, position);
        this.teamName = teamName;
    }

    public PlayerWithTeamName(String firstName, String lastName, int height, int weight, Date birthDate, Country nationality, int teamId, Position position, String teamName) {
        super(firstName, lastName, height, weight, birthDate, nationality, teamId, position);
        this.teamName = teamName;
    }

    public PlayerWithTeamName(int playerId, String firstName, String lastName, int height, int weight, Date birthDate, Country nationality, int teamId, List<Position> positions, String teamName) {
        super(playerId, firstName, lastName, height, weight, birthDate, nationality, teamId, positions);
        this.teamName = teamName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public String toString() {
        return "Player{" +
                "playerId=" + playerId +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", height=" + height +
                ", weight=" + weight +
                ", birthDate=" + birthDate +
                ", nationality=" + nationality +
                ", teamId=" + teamId +
                ", positions=" + positions +
                ", teamName=" + teamName +
                '}';
    }
}
