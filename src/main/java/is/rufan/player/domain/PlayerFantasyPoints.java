package is.rufan.player.domain;

import java.util.Date;
import java.util.List;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class PlayerFantasyPoints {
    private int playerId;
    private float fantasyPoints;

    public PlayerFantasyPoints() {
    }

    public PlayerFantasyPoints(int playerId, float fantasyPoints) {
        this.playerId = playerId;
        this.fantasyPoints = fantasyPoints;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public float getFantasyPoints() {
        return fantasyPoints;
    }

    public void setFantasyPoints(float fantasyPoints) {
        this.fantasyPoints = fantasyPoints;
    }
}
