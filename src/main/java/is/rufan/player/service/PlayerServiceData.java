package is.rufan.player.service;

import is.rufan.player.data.*;
import is.rufan.player.domain.*;
import is.ruframework.data.RuDataAccessFactory;
import is.ruframework.domain.RuException;

import java.util.Collection;
import java.util.List;

public class PlayerServiceData implements PlayerService
{
  RuDataAccessFactory factory;
  PlayerDataGateway playerDataGateway;
  PositionDataGateway positionDataGateway;
  CountryDataGateway countryDataGateway;
  PlayerFantasyPointsDataGateway playerFantasyPointsDataGateway;

  public PlayerServiceData() throws RuException
  {
    factory = RuDataAccessFactory.getInstance("playerdata.xml");
    playerDataGateway = (PlayerDataGateway) factory.getDataAccess("playerData");
    positionDataGateway = (PositionDataGateway) factory.getDataAccess("positionData");
    countryDataGateway = (CountryDataGateway) factory.getDataAccess("countryData");
    playerFantasyPointsDataGateway = (PlayerFantasyPointsDataGateway)
            factory.getDataAccess("playerFantasyPointsData");
  }

  public Player getPlayer(int playerId)
  {
    return playerDataGateway.getPlayer(playerId);
  }

  public List<Player> getPlayers(int teamId)
  {
    return null;
  }

  public List<Player> getPlayersByTeamAbbreviation(int leagueId, String teamAbbreviation)
  {
    return null;
  }

  public List<PlayerWithTeamName> getPlayersByPosition(int positionId) {
    return playerDataGateway.getPlayersByPosition(positionId);
  }

  public void addPlayer(Player player) throws PlayerServiceException
  {
    playerDataGateway.addPlayer(player);

    Country country = countryDataGateway.getCountry(player.getNationality().getCountryId());
    if(country == null)
      countryDataGateway.addCountry(player.getNationality());
  }

  public Collection<Position> getPositions()
  {
    return positionDataGateway.getPositions();
  }

  public Position getPosition(int positionId)
  {
    return positionDataGateway.getPosition(positionId);
  }

  public void addPlayerFantasyPoints(PlayerFantasyPoints playerPoints) {
    playerFantasyPointsDataGateway.addPlayerFantasyPoints(playerPoints);
  }

  public PlayerFantasyPoints getPlayerFantasyPoints(int playerId) {
    PlayerFantasyPoints playerFantasyPoints = null;
    try {
      playerFantasyPoints = playerFantasyPointsDataGateway.getPlayerFantasyPoints(playerId);
    } catch (Exception ex) {
      playerFantasyPoints = new PlayerFantasyPoints(playerId, 0);
    }
    return playerFantasyPoints;
  }
}



