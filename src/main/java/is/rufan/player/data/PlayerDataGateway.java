package is.rufan.player.data;

import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;

import java.util.List;

public interface PlayerDataGateway
{
  void addPlayer(Player player);
  Player getPlayer(int playerid);
  List<PlayerWithTeamName> getPlayersByPosition(int positionId);
}
