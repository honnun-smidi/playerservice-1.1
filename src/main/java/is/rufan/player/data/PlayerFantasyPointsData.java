package is.rufan.player.data;

import is.rufan.player.domain.PlayerFantasyPoints;
import is.ruframework.data.RuData;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class PlayerFantasyPointsData extends RuData implements PlayerFantasyPointsDataGateway {
    public void addPlayerFantasyPoints(PlayerFantasyPoints playerPoints) {
        SimpleJdbcInsert insertPlayerFantasyPoints =
                new SimpleJdbcInsert(getDataSource())
                    .withTableName("playerfantasypoints");

        Map<String, Object> parameters = new HashMap<String, Object>(3);
        parameters.put("playerId", playerPoints.getPlayerId());
        parameters.put("fantasyPoints", playerPoints.getFantasyPoints());

        try {
            insertPlayerFantasyPoints.execute(parameters);
        } catch (DataIntegrityViolationException divex) {
            log.warning(divex.getMessage());
        }
    }

    public PlayerFantasyPoints getPlayerFantasyPoints(int playerId) {
        String sql = "select * from playerfantasypoints where playerId = ?";
        JdbcTemplate queryPlayerPoints = new JdbcTemplate(getDataSource());
        PlayerFantasyPoints playerPoints = queryPlayerPoints.queryForObject(sql,
                new Object[] { playerId }, new PlayerFantasyPointsRowMapper());

        return playerPoints;
    }
}
