package is.rufan.player.data;

import is.rufan.player.domain.Country;
import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerWithTeamName;
import is.rufan.player.domain.Position;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * This mapper is used when getting players by position, e.g. only goalkeepers. Since we are only
 * concerned with one position, a player created by this mapper can only have one position in its
 * positions list. Because we only account for one position for each player, the performance is very
 * good.
 * Created by Snaebjorn on 10/24/2015.
 */
public class PlayerPositionRowMapper implements RowMapper<PlayerWithTeamName> {
    public PlayerWithTeamName mapRow(ResultSet rs, int i) throws SQLException {
        PlayerWithTeamName player = new PlayerWithTeamName();
        player.setPlayerId(rs.getInt("playerid"));
        player.setFirstName(rs.getString("firstname"));
        player.setLastName(rs.getString("lastname"));
        player.setHeight(rs.getInt("height"));
        player.setWeight(rs.getInt("weight"));
        player.setBirthDate(rs.getDate("birthdate"));
        player.setTeamId(rs.getInt("teamid"));
        player.setTeamName(rs.getString("teamname"));
        // get players nationality from row
        player.setNationality(new Country(rs.getInt("countryid"), rs.getString("countryname"), rs.getString("countryabbreviation")));
        // get players position from row
        List<Position> positions = new ArrayList<Position>();
        positions.add(new Position(rs.getInt("positionid"), rs.getString("positionname"), rs.getString("positionabbreviation")));
        player.setPositions(positions);

        return player;
    }
}
