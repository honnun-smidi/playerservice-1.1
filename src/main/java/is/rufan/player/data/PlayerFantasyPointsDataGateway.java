package is.rufan.player.data;

import is.rufan.player.domain.PlayerFantasyPoints;

/**
 * Created by Snaebjorn on 10/24/2015.
 */
public interface PlayerFantasyPointsDataGateway {
    public void addPlayerFantasyPoints(PlayerFantasyPoints player);
    public PlayerFantasyPoints getPlayerFantasyPoints(int playerId);
}
