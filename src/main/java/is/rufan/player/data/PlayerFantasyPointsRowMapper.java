package is.rufan.player.data;

import is.rufan.player.domain.Player;
import is.rufan.player.domain.PlayerFantasyPoints;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by Snaebjorn on 10/24/2015.
 */
public class PlayerFantasyPointsRowMapper implements RowMapper<PlayerFantasyPoints> {
    public PlayerFantasyPoints mapRow(ResultSet rs, int i) throws SQLException {
        PlayerFantasyPoints playerPoints = new PlayerFantasyPoints();
        playerPoints.setPlayerId(rs.getInt("playerId"));
        playerPoints.setFantasyPoints(rs.getFloat("fantasyPoints"));

        return playerPoints;
    }
}
