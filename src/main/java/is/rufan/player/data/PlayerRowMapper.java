package is.rufan.player.data;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import is.rufan.player.domain.Country;
import is.rufan.player.domain.Player;
import is.rufan.player.domain.Position;
import is.rufan.player.service.PlayerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import javax.sql.DataSource;

/**
 * This mapper is used when getting a player and we want all of his positions. Its
 * performance for this is ok when just getting one player, but horrible when getting
 * many players. Beware of this.
 */
public class PlayerRowMapper implements RowMapper<Player>
{
  private DataSource dataSource;

  public PlayerRowMapper(DataSource dataSource) {
    ApplicationContext ctx = new FileSystemXmlApplicationContext("classpath:playerapp.xml");
    this.dataSource = dataSource;
  }

  public Player mapRow(ResultSet rs, int rowNum) throws SQLException
  {
    Player player = new Player();
    player.setPlayerId(rs.getInt("playerid"));
    player.setFirstName(rs.getString("firstname"));
    player.setLastName(rs.getString("lastname"));
    player.setHeight(rs.getInt("height"));
    player.setWeight(rs.getInt("weight"));
    player.setBirthDate(rs.getDate("birthdate"));
    player.setTeamId(rs.getInt("teamid"));
    // get players nationality from row
    player.setNationality(new Country(rs.getInt("countryid"), rs.getString("name"), rs.getString("abbreviation")));

    // get player positions with a new query, add positions to player
    String sql = "select * from playerpositions pp " +
            "join positions p on pp.positionid = p.positionid " +
            "where playerid = ?";

    JdbcTemplate queryPositions = new JdbcTemplate(dataSource);
    List<Position> positions = queryPositions.query(sql,
            new Object[] { player.getPlayerId() }, new PositionRowMapper());

    player.setPositions(positions);

    return player;
  }
}